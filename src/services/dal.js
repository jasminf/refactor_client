import { client } from "../apollo-client";
import { GET_APP_CONFIGURATION } from "../queries/getConfiguration";

export const getAppConfiguration = () => {
    return client.query({
        query:GET_APP_CONFIGURATION,
        variables:{}
    })
}