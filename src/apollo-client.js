import ApolloClient from 'apollo-client';
import { createHttpLink } from 'apollo-link-http';
import { ApolloLink, from } from 'apollo-link';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { onError } from 'apollo-link-error';

const httpLink = createHttpLink({ uri: process.env.REACT_APP_API_URL, credentials: 'include' });
// Prepare authentication credentials
const credentials = new Buffer(process.env.REACT_APP_NEO4J_UID + ':' + process.env.REACT_APP_NEO4J_PWD).toString('base64');

// Make middleware for authentication purposes
const authMiddleware = new ApolloLink((operation, forward) => {
  // Add authorization to the headers
  operation.setContext({
    headers: {
      Authorization: 'Basic ' + credentials,
      //apiVersion: getApiVersionFromSession(),
    },
  });

  return forward(operation);
});

// Make global exception handler
const errorHandler = onError(({ graphQLErrors, networkError }) => {
  let apiVersion = null;
  let cookieFailed = null;
  if (graphQLErrors) {
    for (let i = 0; i < graphQLErrors.length; i++) {
      // Handle missing or different apiVersions error
      if (graphQLErrors[i].statusCode === 1020 && graphQLErrors[i].message) {
        apiVersion = graphQLErrors[i].message;
        break;
      } else if (graphQLErrors[i].statusCode === 1030 && graphQLErrors[i].message) {
        cookieFailed = graphQLErrors[i].message;
        break;
      }
    }
  }

  if (apiVersion) {
    // Set new API version
    //setApiVersionToSession(apiVersion);
    //setForceReloadFlag();
    window.location.reload();
  } else if (cookieFailed) {
    // Return user to landing page if no cookie.
    window.localStorage.removeItem('data');
  }
});

//DISABLE CACHE IF NEEDED
const defaultOptions = {
  watchQuery: {
    fetchPolicy: 'network-only',
    errorPolicy: 'ignore',
  },
  query: {
    fetchPolicy: 'network-only',
    errorPolicy: 'all',
  },
};

// Configure apollo client
export const client = new ApolloClient({
  link: from([authMiddleware, errorHandler, httpLink]),
  cache: new InMemoryCache(),
  defaultOptions: defaultOptions,
});
