import { combineReducers } from 'redux';
import configurationReducer from './configuration';

const rootReducer = combineReducers({
    configurationReducer
});

export default rootReducer;
