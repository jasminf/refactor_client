const initialState = {
  configuration:null
};

const configurationReducer = (state = initialState, action) => {
  switch (action.type) {
    case "SET_CONFIGURATION":
      return {
        ...state,
        configuration: action.payload,
      }
    default:
      return state;
  }
};

export default configurationReducer;