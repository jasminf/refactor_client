import React from "react";
import * as JSONX from "jsonx";
import { connect } from "react-redux";
import Loader from "../components/Loader";

class Main extends React.Component {
    render() {
        return this.props.configuration ? JSONX.getReactElement(JSON.parse(this.props.configuration)) : <Loader />;
    }
}

const mapStateToProps = state => {
    return {
        configuration: state.configurationReducer.configuration
    };
  };
  const mapDispatchToProps = dispatch => {
    return {
    
    };
  };

export default connect(mapStateToProps,mapDispatchToProps)(Main);