import { getAppConfiguration } from "../services/dal"

export const getAndSetConfiguration = () => dispatch =>{
    getAppConfiguration().then(res=>{
        dispatch(setAppConfiguration(res.data.getAppConfiguration))
    })
}

const setAppConfiguration = (data) => ({
    type:"SET_CONFIGURATION",
    payload:data
})