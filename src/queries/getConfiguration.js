import gql from 'graphql-tag';

export const GET_APP_CONFIGURATION = gql`
  query getAppConfiguration {
    getAppConfiguration
  }
`;