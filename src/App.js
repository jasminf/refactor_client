import React, {Fragment} from 'react';
import './App.css';
import Main from "./core/Main"
import { getAndSetConfiguration } from './actions/configuration';
import { connect } from 'react-redux';

class App extends React.Component {
  
  componentDidMount() {
    //fetch schema
    this.props.getAndSetConfiguration()
  }

  render() {
    return(
      <Fragment>
         <Main />
      </Fragment>
    )
  }
}



const mapStateToProps = state => {
  return {
  };
};
const mapDispatchToProps = dispatch => {
  return {
    getAndSetConfiguration: () => dispatch(getAndSetConfiguration())
  };
};


export default connect(mapStateToProps,mapDispatchToProps)(App);

